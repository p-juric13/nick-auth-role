<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

Route::group(array(
	'prefix' => 'api/users',
	'before' => 'api.auth',
	'after' => 'api.auth.extend'
), function () {
	
	// Find users
	Route::get('', array(
		'before' => 'api.admin',
		'uses' => 'ApiUsersController@find'
	));
	
	// Get user by id
	Route::get('{id}', array(
		'uses' => 'ApiUsersController@getById'
	))->where(array('id' => '[0-9]+'));
	
	// Get me
	Route::get('me', array(
		'uses' => 'ApiUsersController@getMe'
	));
	
	// Create user
	Route::post('', array(
		'before' => 'api.admin',
		'uses' => 'ApiUsersController@create'
	))->where(array('id' => '[0-9]+'));
	
	// Update user by id
	Route::put('{id}', array(
		'uses' => 'ApiUsersController@update'
	));
	
	// Update me
	Route::put('me', array(
		'uses' => 'ApiUsersController@update'
	));
	
	// Delete user by id
	Route::delete('{id}', array(
		'before' => 'api.admin',
		'uses' => 'ApiUsersController@delete'
	))->where(array('id' => '[0-9]+'));
	
});

Route::group(array(
	'prefix' => 'api/search',
	'before' => 'api.auth',
	'after' => 'api.auth.extend'
), function () {
	// Find users 
	Route::get('users', array(
		'uses' => 'ApiUsersController@search',
	));

	// Find users autocomplete 
	Route::get('users/autocomplete', array(
		'uses' => 'ApiUsersController@autocomplete',
	));
});

Route::group(array('prefix' => 'api/auth'), function () {

	// Login
	Route::post('login', array(
		'uses' => 'ApiAuthController@login'
	));
	
	// Register
	Route::post('register', array(
		'uses' => 'ApiAuthController@register'
	));
	
	// Logout
	Route::get('logout', array(
		'before' => 'api.auth',
		'uses' => 'ApiAuthController@logout'
	));

});