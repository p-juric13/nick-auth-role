<!DOCTYPE html>
<html lang="en" ng-app="App">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
		<meta name="author" content="Le Dang Duong fb.com/leduongcom">
		<title>Laravel4 AngularJS Authentication</title>
		<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/app.css" rel="stylesheet">
	</head>
	<body data-ng-controller="AppCtrl">
		<header data-ng-hide="isSpecificPage()" data-ng-cloak="" class="no-print">
			<section data-ng-include=" 'html/header.html' " id="header" class="top-header"></section>
			<aside data-ng-include=" 'html/nav.html' " id="nav-container"></aside>
		</header>
		<!-- /header -->
		<div class="view-container">
			<div data-ng-view="" id="main-content" class="animate-fade-up"></div>
		</div>
		<!-- /main-content -->
		<footer app-version="">
		</footer>
		<!-- /footer -->
	</body>
	<script src="js/vendor.js"></script>
	<script src="js/init.js"></script>
	<script>
	angular.module("App").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
	</script>
</html>