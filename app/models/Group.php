<?php

class Group extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users_groups';
    protected $fillable = array('id', 'name', 'description', 'creation_date');

    public function users()
    {
        return $this->hasMany('User');
    }

}

