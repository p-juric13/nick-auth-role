<?php

class UserRole extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users_roles';
	protected $fillable = array(
		'user_id',
		'permission');

	public function user()
	{
		return $this->belongsTo('User');
	}

}
