<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('deleted_at');
	protected $fillable = array('group_id');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function credential()
	{
		return $this->hasOne('UserCredential');
	}

	public function meta()
	{
		return $this->hasOne('UserMeta');
	}

	public function group()
	{
		return $this->belongsTo('Group');
	}

	public function isAdmin() {
		if ($this->group->name == 'admin') {
			return true;
		}

		return false;
	}

	public function fullName() {
		$fullName = array();
		if ($this->meta->first_name != "") {
			$fullName[] = $this->meta->first_name;
		}
		if ($this->meta->last_name != "") {
			$fullName[] = $this->meta->last_name;
		}

		return implode(" ", $fullName);
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}
}
