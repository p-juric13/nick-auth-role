<?php

class UserDeveice extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users_device';
	protected $fillable = array(
		'user_id',
		'name',
		'activation_date',
		'serial_number',
		'mac_address',
		'local_ip_address',
		'global_ip_address',
		'port',
		'time_zone');

	public function user()
	{
		return $this->belongsTo('User');
	}

}
