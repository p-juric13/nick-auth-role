<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('users_groups');
		Schema::create('users_groups', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')->unsigned();
			$table->string('name', 32)->unique();
			$table->string('description', 64);
			$table->timestamps();
		});

		Schema::dropIfExists('users');
		Schema::create('users', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')->unsigned();
			$table->integer('group_id')->unsigned();
			$table->string('email')->unique();
			$table->string('password', 64);
			$table->enum('status', array('pending', 'active', 'inactive', 'banned'))->default('pending');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('group_id')->references('id')->on('users_groups');
		});

		Schema::dropIfExists('users_meta');
		Schema::create('users_meta', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->integer('user_id')->unsigned();

			$table->string('first_name', 64);
			$table->string('last_name', 64);
			$table->string('phone', 16);
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
		Schema::dropIfExists('users_device');
		Schema::create('users_device', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';

			$table->increments('id')->unsigned();
			$table->integer('user_id')->unsigned();

			$table->string('name', 64)->unique();
			$table->string('serial_number', 64);
			$table->string('mac_address', 64)->unique();
			$table->string('local_ip_address', 64);
			$table->string('global_ip_address', 64);
			$table->string('port', 8);
			$table->string('time_zone', 64);
			$table->timestamp('activation_date')->default('0000-00-00 00:00:00');
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('users_meta');
		Schema::dropIfExists('users_device');
		Schema::dropIfExists('users_groups');
	}

}
