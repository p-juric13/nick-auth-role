<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Credentials extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('users_credentials');
		Schema::create('users_credentials', function(Blueprint $table)
		{
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('email')->unique();
            $table->string('password', 64);
            $table->timestamps();
		});

        // migrate
        //DB::select(DB::raw('insert into `users_credentials` (`user_id`,`email`,`password`) (select `id`,`email`,`password` from `users`)'));

        //DB::select(DB::raw('update `users_meta` inner join `users` on (`users`.`id` = `users_meta`.`user_id`) set `users_meta`.`email` = `users`.`email`'));

		Schema::table('users', function(Blueprint $table)
		{
            $table->dropUnique('users_email_unique');
            $table->dropColumn('password');
            $table->dropColumn('email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->string('email')->unique();
            $table->string('password', 64);
		});

        DB::select(DB::raw('update `users` inner join users_credentials on (`users`.`id` = `users_credentials.user_id`) set `users`.`username` = `user_credentials`.`username`, `users`.`password` = `user_credentials`.`password`'));

        DB::select(DB::raw('update `users` inner join `users_meta` on (`users_meta`.`id` = `users`.`user_id`) set `users`.`email` = `users_meta`.`email`'));

		Schema::table('users_meta', function(Blueprint $table)
		{
            $table->dropColumn('email');
		});

        DB::select(DB::raw('drop table user_credentials'));
	}

}
