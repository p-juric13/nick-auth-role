<?php
 
class UserTableSeeder extends Seeder {
 
	public function run()
	{
		DB::table('users')->delete();

		$admin_group = Group::where('name', 'admin')->first();
		$member_group = Group::where('name', 'member')->first();
 
		$admin_user = User::create(array(
			'group_id' => $admin_group->id,
			'status' => 'active'
		));
		UserCredential::create(array(
			'user_id' => $admin_user->id,
			'email' => 'admin@example.com',
			'password' => Hash::make('password'),
		));
		UserMeta::create(array(
			'user_id' => $admin_user->id,
			'first_name' => 'John',
			'last_name' => 'Admin'
		));
 
		$member_user = User::create(array(
			'group_id' => $member_group->id,
			'status' => 'active'
		));
		UserCredential::create(array(
			'user_id' => $member_user->id,
			'email' => 'member@example.com',
			'password' => Hash::make('password'),
		));
		UserMeta::create(array(
			'user_id' => $member_user->id,
			'first_name' => 'John',
			'last_name' => 'Member'
		));
	}
 
}

