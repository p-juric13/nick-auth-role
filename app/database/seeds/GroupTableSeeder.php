<?php
 
class GroupTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('users_groups')->delete();

        $admin_group = Group::create(array(
            'id' => 1,
            'name' => 'admin',
            'description' => 'Admin users group'
        ));

        $member_group = Group::create(array(
            'id' => 2,
            'name' => 'member',
            'description' => 'Member users group'
        ));

        $member_group = Group::create(array(
            'id' => 3,
            'name' => 'default',
            'description' => 'Default users group'
        ));
    }

}
