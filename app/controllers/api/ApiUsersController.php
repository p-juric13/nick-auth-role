<?php

class ApiUsersController extends BaseApiController {

    /**
     *  @param  page    optional    page number
     *  @param  limit   optional    page limit
     *
     *  @desc Find users
     **/
    public function find()
    {
        $authToken = App::make('authToken');
        $currentUser = $authToken->user;

        $limit = Input::get('limit') ? (int)Input::get('limit') : Config::get('restful.defaults.pagination.limit');
        $sort = Input::get('sort') ? Input::get('sort') : 'id'; 
        $order = Input::get('order') ? Input::get('order') : 'asc'; 
 
        $query = User::with('credential', 'meta', 'group');
        if (!$currentUser->isAdmin()) {
            $query->where('users.status', 'active'); // only actives
            $query->where('users.group_id', $currentUser->group_id); // member group can only search member
            $query->where("users.id", "!=", $currentUser->id);
        }
        $query->orderBy($sort, $order);
        
        $users = $query->paginate($limit);

        if ($users) {
            $resp = RestResponseFactory::ok($users->toArray());
        } else {
            $resp = RestResponseFactory::ok(array(), "User(s) not found.");
        }
        return Response::json($resp);
    }

    /**
     *  @param  page    optional    page number
     *  @param  limit   optional    page limit
     *
     *  @desc Search users
     **/
    public function search()
    {
        $authToken = App::make('authToken');
        $currentUser = $authToken->user;

        $limit = Input::get('limit') ? (int)Input::get('limit') : Config::get('restful.defaults.pagination.limit');
        $sort = Input::get('sort') ? Input::get('sort') : 'users.id'; 
        $order = Input::get('order') ? Input::get('order') : 'asc'; 
        $q = Input::get('q') ? Input::get('q') : null; 
        //$uri = Request::path();
 
        // stats query
        $query = DB::table("users")
            ->select(DB::raw("users.id, user_credentials.username, users.status as user_status, groups.name as group_name, user_meta.profile_img_url, user_meta.email, user_meta.first_name, user_meta.last_name, MAX(auth_tokens.created_at) as last_login"))
            ->leftJoin("user_meta", "user_meta.user_id", "=", "users.id")
            ->leftJoin("user_credentials", "user_credentials.user_id", "=", "users.id")
            ->leftJoin("groups", "groups.id", "=", "users.group_id")
            ->leftJoin("auth_tokens", "auth_tokens.user_id", "=", "users.id");

        if ($q) {
            $query->whereRaw("CONCAT_WS(' ', `user_meta`.`first_name`, `user_meta`.`last_name`) like ?", array('%'.$q.'%'));
        }
        if (!$currentUser->isAdmin()) {
            $query->where('users.group_id', $currentUser->group_id); // member group can only search member
            $query->where('users.status', 'active'); // only actives
            $query->where("users.id", "!=", $currentUser->id);
        } else {
            if ($status = Input::get('status')) {
                $query->where('users.status', $status);
            }
        }
        $query->groupBy("users.id");
        $query->orderBy($sort, $order);
        
        $users = $query->paginate($limit);
        if ($users) {
            $resp = RestResponseFactory::ok($users->toArray());
        } else {
            $resp = RestResponseFactory::ok(array(), "User(s) not found.");
        }
        return Response::json($resp);
    }

    /**
     *  @desc autocomplete
     **/
    public function autocomplete()
    {
        $authToken = App::make('authToken');
        $currentUser = $authToken->user;

        $validator = Validator::make(
            Input::all(),
            array(
                'q' => 'required|min:2',
            )
        );
        if ($validator->fails())
        {
            $resp = RestResponseFactory::badrequest(null, "<ul>".implode("", $validator->messages()->all('<li>:message</li>'))."</ul>");
            return Response::json($resp);
        }

        $q = Input::get('q');
        $query = DB::table("users")
            ->select(DB::raw("`users`.`id` as id, CONCAT_WS(' ', user_meta.`first_name`, user_meta.`last_name`) as name"))
            ->leftJoin("user_meta", "user_meta.user_id", "=", "users.id");

        if ($q) {
            $query->whereRaw("CONCAT_WS(' ', `user_meta`.`first_name`, `user_meta`.`last_name`) like ?", array('%'.$q.'%'));
        }
        if (!$currentUser->isAdmin()) {
            $query->where('users.group_id', $currentUser->group_id); // member group can only search member
            $query->where('users.status', 'active'); // only actives
            $query->where("users.id", "!=", $currentUser->id);
        } else {
            if ($status = Input::get('status')) {
                $query->where('users.status', $status);
            }
        }
        $query->orderBy('name', 'asc');
        $results = $query->get();

        if ($results) {
            $resp = RestResponseFactory::ok($results);
        } else {
            $resp = RestResponseFactory::ok(array(), "User(s) not found.");
        }
        return Response::json($resp);
    }


    /**
     *  @desc Get user by id
     **/
    public function getById($id)
    {
        $authToken = App::make('authToken');
        if ($authToken->user->isAdmin()) {
            $user = User::with('credential', 'meta', 'group', 'billing')->find($id); // show billing
        } else {
            $user = User::with('credential', 'meta', 'group')->find($id);
        }

        if ($user) {
            $resp = RestResponseFactory::ok($user->toArray());
        } else {
            $resp = RestResponseFactory::ok(null, "User not found.");
        }
        return Response::json($resp);
    }


    /**
     *  @desc Get me
     **/
    public function getMe()
    {
        $authToken = App::make('authToken');
        $currentUser = $authToken->user;
        $currentUser->credential;
        $currentUser->meta;
        $currentUser->group;
        $currentUser->billing;

        if ($currentUser) {
            $resp = RestResponseFactory::ok($currentUser->toArray());
        } else {
            $resp = RestResponseFactory::ok(null, "User not found.");
        }
        return Response::json($resp);
    }


    /**
     *  @desc Create user 
     **/
    public function create()
    {
        $requestBody = file_get_contents('php://input');
        $request = json_decode($requestBody, true);
        
        $errors = array();
        $validator = Validator::make(
            $request,
            array(
                'group_id' => 'required|exists:groups,id',
                'status' => 'required|in:pending,active,inactive,banned'
            )
        );
        if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
        $validator = Validator::make(
            isset($request['meta']) ? $request['meta'] : array(),
            array(
                'email' => 'required|email',
                'first_name' => 'required',
                'last_name' => 'required',
                //'address1' => 'required',
                //'city' => 'required',
                //'province' => 'required',
                //'postal' => 'required',
                //'country' => 'required',
                //'phone' => 'required',
            )
        );
        if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
        $validator = Validator::make(
            isset($request['credential']) ? $request['credential'] : array(),
            array(
                'username' => 'required|alpha_dash|min:5|max:16|unique:user_credentials',
                'password' => 'required|min:5',
            )
        );
        if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
        if (count($errors) > 0) {
            $resp = RestResponseFactory::badrequest(null, "<ul>".implode("", $errors)."</ul>");
            return Response::json($resp);
        }
        
        $user = new User();
        $user->group_id = $request['group_id'];
        $user->status = $request['status'];
        $user->save();

        $userCredential = new UserCredential();
        $userCredential->user_id = $user->id;
        $userCredential->username = $request['credential']['username'];
        $userCredential->password = Hash::make($request['credential']['password']);
        $userCredential->save();

        $userMeta = new UserMeta();
        $userMeta->user_id = $user->id;
        $userMeta->email = $request['meta']['email'];
        $userMeta->first_name = isset($request['meta']['first_name']) ? $request['meta']['first_name'] : "";
        $userMeta->last_name = isset($request['meta']['last_name']) ? $request['meta']['last_name'] : "";
        $userMeta->address1 = isset($request['meta']['address1']) ? $request['meta']['address1'] : "";
        $userMeta->address2 = isset($request['meta']['address2']) ? $request['meta']['address2'] : "";
        $userMeta->country = isset($request['meta']['country']) ? $request['meta']['country'] : "";
        $userMeta->province = isset($request['meta']['province']) ? $request['meta']['province'] : "";
        $userMeta->city = isset($request['meta']['city']) ? $request['meta']['city'] : "";
        $userMeta->postal = isset($request['meta']['postal']) ? $request['meta']['postal'] : "";
        $userMeta->fax = isset($request['meta']['fax']) ? $request['meta']['fax'] : "";
        $userMeta->phone = isset($request['meta']['phone']) ? $request['meta']['phone'] : "";
        $userMeta->company = isset($request['meta']['company']) ? $request['meta']['company'] : "";
        $userMeta->website_url = isset($request['meta']['website_url']) ? $request['meta']['website_url'] : "";
        $userMeta->profile_img_url = isset($request['meta']['profile_img_url']) ? $request['meta']['profile_img_url'] : "";
        $userMeta->save();

        $userBilling = new UserBilling();
        $userBilling->user_id = $user->id;
        $userBilling->credit_card_name = isset($request['billing']['credit_card_name']) ? $request['billing']['credit_card_name'] : "";
        $userBilling->credit_card_num = isset($request['billing']['credit_card_num']) ? $request['billing']['credit_card_num'] : "";
        $userBilling->credit_card_type = isset($request['billing']['credit_card_type']) ? $request['billing']['credit_card_type'] : "";
        $userBilling->credit_card_expiry_month = isset($request['billing']['credit_card_expiry_month']) ? $request['billing']['credit_card_expiry_month'] : "";
        $userBilling->credit_card_expiry_year = isset($request['billing']['credit_card_expiry_year']) ? $request['billing']['credit_card_expiry_year'] : "";
        $userBilling->credit_card_ccv = isset($request['billing']['credit_card_ccv']) ? $request['billing']['credit_card_ccv'] : "";
        $userBilling->same_as_profile = isset($request['billing']['same_as_profile']) ? $request['billing']['same_as_profile'] : "";
        $userBilling->address1 = isset($request['billing']['address1']) ? $request['billing']['address1'] : "";
        $userBilling->address2 = isset($request['billing']['address2']) ? $request['billing']['address2'] : "";
        $userBilling->country = isset($request['billing']['country']) ? $request['billing']['country'] : "";
        $userBilling->province = isset($request['billing']['province']) ? $request['billing']['province'] : "";
        $userBilling->city = isset($request['billing']['city']) ? $request['billing']['city'] : "";
        $userBilling->postal = isset($request['billing']['postal']) ? $request['billing']['postal'] : "";
        $userBilling->save();

        NotificationService::newUser($user);

        $resp = RestResponseFactory::ok($user->toArray());
        return Response::json($resp);
    }

    /**
     *  @desc Update user
     **/
    public function update($id)
    {
        $authToken = App::make('authToken');
        $currentUser = $authToken->user;
        if ($id == 'me') {
            $id = $currentUser->id;
        } else {
            if (!$currentUser->isAdmin()) {
                $resp = RestResponseFactory::forbidden("", "Can't update other user.");
                return Response::json($resp);
            }
        }

        $requestBody = file_get_contents('php://input');
        $request = json_decode($requestBody, true);

        $user = User::with('credential', 'meta', 'group', 'billing')->find($id);
        if (!$user) {
            $resp = RestResponseFactory::ok(null, "User not found.");
            return Response::json($resp);
        }

        $errors = array();
        $validator = Validator::make(
            $request,
            array(
                'group_id' => 'exists:groups,id',
                'status' => 'in:pending,active,inactive,banned'
            )
        );
        if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
        $validator = Validator::make(
            isset($request['meta']) ? $request['meta'] : array(),
            array(
                'email' => 'email',
                //'first_name' => 'required',
                //'last_name' => 'required',
                //'address1' => 'required',
                //'city' => 'required',
                //'province' => 'required',
                //'postal' => 'required',
                //'country' => 'required',
                //'phone' => 'required',
            )
        );
        if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
        $validator = Validator::make(
            isset($request['credential']) ? $request['credential'] : array(),
            array(
                'username' => 'alpha_dash|min:5|max:16|unique:user_credentials,username,' . $user->id,
                'password' => 'min:5',
            )
        );
        if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
        if (count($errors) > 0) {
            $resp = RestResponseFactory::badrequest(null, "<ul>".implode("", $errors)."</ul>");
            return Response::json($resp);
        }

        $userCredential = $user->credential;
        $userCredential->username = isset($request['credential']['username']) ? $request['credential']['username'] : $userCredential->username; 
        if (isset($request['credential']['password']) && $request['credential']['password'] != "") {
            $user->password = Hash::make($request['credential']['password']);
        }
        if ($currentUser->isAdmin()) {
            $user->group_id = isset($request['group_id']) ? $request['group_id'] : $user->group_id;
            $previousStatus = $user->status;
            $user->status = isset($request['status']) ? $request['status'] : $user->status;
        }
        $user->save();

        if ($previousStatus == 'pending' && $user->status == 'active') {
            NotificationService::userActivation($user);
        }

        $userMeta = $user->meta;
        $userMeta->user_id = $user->id;
        $userMeta->email = isset($request['meta']['email']) ? $request['meta']['email'] : $userMeta->email;
        $userMeta->first_name = isset($request['meta']['first_name']) ? $request['meta']['first_name'] : $userMeta->first_name;
        $userMeta->last_name = isset($request['meta']['last_name']) ? $request['meta']['last_name'] : $userMeta->last_name;
        $userMeta->address1 = isset($request['meta']['address1']) ? $request['meta']['address1'] : $userMeta->address1;
        $userMeta->address2 = isset($request['meta']['address2']) ? $request['meta']['address2'] : $userMeta->address2;
        $userMeta->country = isset($request['meta']['country']) ? $request['meta']['country'] : $userMeta->country;
        $userMeta->province = isset($request['meta']['province']) ? $request['meta']['province'] : $userMeta->province;
        $userMeta->city = isset($request['meta']['city']) ? $request['meta']['city'] : $userMeta->city;
        $userMeta->postal = isset($request['meta']['postal']) ? $request['meta']['postal'] : $userMeta->postal;
        $userMeta->fax = isset($request['meta']['fax']) ? $request['meta']['fax'] : $userMeta->fax;
        $userMeta->phone = isset($request['meta']['phone']) ? $request['meta']['phone'] : $userMeta->phone;
        $userMeta->company = isset($request['meta']['company']) ? $request['meta']['company'] : $userMeta->company;
        $userMeta->website_url = isset($request['meta']['website_url']) ? $request['meta']['website_url'] : $userMeta->website_url;
        $userMeta->profile_img_url = isset($request['meta']['profile_img_url']) ? $request['meta']['profile_img_url'] : $userMeta->profile_img_url;
        $userMeta->save();

        $userBilling = $user->billing;
        $userBilling->user_id = $user->id;
        $userBilling->credit_card_name = isset($request['billing']['credit_card_name']) ? $request['billing']['credit_card_name'] : $userBilling->credit_card_name;
        $userBilling->credit_card_num = isset($request['billing']['credit_card_num']) ? $request['billing']['credit_card_num'] : $userBilling->credit_card_num;
        $userBilling->credit_card_type = isset($request['billing']['credit_card_type']) ? $request['billing']['credit_card_type'] : $userBilling->credit_card_type;
        $userBilling->credit_card_expiry_month = isset($request['billing']['credit_card_expiry_month']) ? $request['billing']['credit_card_expiry_month'] : $userBilling->credit_card_expiry_month;
        $userBilling->credit_card_expiry_year = isset($request['billing']['credit_card_expiry_year']) ? $request['billing']['credit_card_expiry_year'] : $userBilling->credit_card_expiry_yearh;
        $userBilling->credit_card_ccv = isset($request['billing']['credit_card_ccv']) ? $request['billing']['credit_card_ccv'] : $userBilling->credit_card_ccv;
        $userBilling->same_as_profile = isset($request['billing']['same_as_profile']) ? $request['billing']['same_as_profile'] : $userBilling->same_as_profile;
        $userBilling->address1 = isset($request['billing']['address1']) ? $request['billing']['address1'] : $userBilling->address1;
        $userBilling->address2 = isset($request['billing']['address2']) ? $request['billing']['address2'] : $userBilling->address2;
        $userBilling->country = isset($request['billing']['country']) ? $request['billing']['country'] : $userBilling->country;
        $userBilling->province = isset($request['billing']['province']) ? $request['billing']['province'] : $userBilling->province;
        $userBilling->city = isset($request['billing']['city']) ? $request['billing']['city'] : $userBilling->city;
        $userBilling->postal = isset($request['billing']['postal']) ? $request['billing']['postal'] : $userBilling->postal;
        $userBilling->save();

        $resp = RestResponseFactory::ok($user->toArray());
        return Response::json($resp);
    }

    /**
     *  @desc Delete user
     **/
    public function delete($id)
    {
        $requestBody = file_get_contents('php://input');
        $request = json_decode($requestBody);
        
        $user = User::find($id);

        if ($user) {
            $user->delete(); 
            $resp = RestResponseFactory::ok(null);
        } else {
            $resp = RestResponseFactory::ok(null, "User not found.");
        }
        return Response::json($resp);
    }

    /**
     *  @desc Upload profile image 
     **/
    public function uploadProfileImage($id) {
        if ($id == 'me') {
            $authToken = App::make('authToken');
            $id = $authToken->user_id;
        } 
        $user = User::with('meta')->find($id);
        
        if ($user) {
            if (Input::hasFile('img'))
            {
                $extension = Input::file('img')->getClientOriginalExtension();
                $filename = $user->username . '.' . $extension;
                Input::file('img')->move(Config::get('restful.paths.profile_img'), $filename);
            }

            $userMeta = $user->meta;
            $userMeta->profile_img_url = Config::get('restful.urls.profile_img') . '/' . $filename;
            $userMeta->save();

            $resp = RestResponseFactory::ok($userMeta->profile_img_url);
        } else {
            $resp = RestResponseFactory::ok(null, "User not found.");
        }
        return Response::json($resp);
    }
}
