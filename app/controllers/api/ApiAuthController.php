<?php

class ApiAuthController extends BaseApiController {

	/**
	 *  @desc Login
	 **/
	public function login()
	{
		$requestBody = file_get_contents('php://input');
		$request = json_decode($requestBody);

		if ($authToken = AuthToken::login($request->username, $request->password)) {
			$resp = RestResponseFactory::ok($authToken->toArray());
		} else {
			$resp = RestResponseFactory::badrequest(null, "Invalid Username/Password");
		}
		return Response::json($resp);
	}

	/**
	 *  @desc Logout
	 **/
	public function logout()
	{
		$authToken = App::make('authToken');
		$authToken->expired_at = date('Y-m-d H:i:s');
		$authToken->updated_at = $authToken->expired_at;
		$authToken->save();
		$resp = RestResponseFactory::ok(null);
		return Response::json($resp);
	}

	/**
	 *  @desc Register user 
	 **/
	public function register()
	{
		$requestBody = file_get_contents('php://input');
		$request = json_decode($requestBody, true);

		$errors = array();
		$validator = Validator::make(
			isset($request['meta']) ? $request['meta'] : array(),
			array(
				'email' => 'required|email',
				'first_name' => 'required',
				'last_name' => 'required',
			)
		);
		if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
		$validator = Validator::make(
			isset($request['credential']) ? $request['credential'] : array(),
			array(
				'username' => 'required|alpha_dash|min:5|max:16|unique:user_credentials',
				'password' => 'required|min:5',
			)
		);
		if ($validator->fails()) $errors = array_merge($errors, $validator->messages()->all('<li>:message</li>'));
		if (count($errors) > 0) {
			$resp = RestResponseFactory::badrequest(null, "<ul>".implode("", $errors)."</ul>");
			return Response::json($resp);
		}

		$group = Group::where('name', Config::get('restful.defaults.group.name'))->first();
		
		$user = new User();
		$user->group_id = $group->id;
		$user->status = Config::get('restful.defaults.user.status');
		$user->save();
		
		$userCredential = new UserCredential();
		$userCredential->user_id = $user->id;
		$userCredential->username = $request['credential']['username'];
		$userCredential->password = Hash::make($request['credential']['password']);
		$userCredential->save();

		$userMeta = new UserMeta();
		$userMeta->user_id = $user->id;
		$userMeta->email = $request['meta']['email'];
		$userMeta->first_name = isset($request['meta']['first_name']) ? $request['meta']['first_name'] : "";
		$userMeta->last_name = isset($request['meta']['last_name']) ? $request['meta']['last_name'] : "";
		$userMeta->address1 = isset($request['meta']['address1']) ? $request['meta']['address1'] : "";
		$userMeta->address2 = isset($request['meta']['address2']) ? $request['meta']['address2'] : "";
		$userMeta->country = isset($request['meta']['country']) ? $request['meta']['country'] : "";
		$userMeta->province = isset($request['meta']['province']) ? $request['meta']['province'] : "";
		$userMeta->city = isset($request['meta']['city']) ? $request['meta']['city'] : "";
		$userMeta->postal = isset($request['meta']['postal']) ? $request['meta']['postal'] : "";
		$userMeta->fax = isset($request['meta']['fax']) ? $request['meta']['fax'] : "";
		$userMeta->phone = isset($request['meta']['phone']) ? $request['meta']['phone'] : "";
		$userMeta->company = isset($request['meta']['company']) ? $request['meta']['company'] : "";
		$userMeta->website_url = isset($request['meta']['website_url']) ? $request['meta']['website_url'] : "";
		$userMeta->profile_img_url = isset($request['meta']['profile_img_url']) ? $request['meta']['profile_img_url'] : "";
		$userMeta->save();

		// Notification
		NotificationService::newUser($user);

		$authToken = AuthToken::loginAsUser($user->id);
		if ($authToken) {
			$resp = RestResponseFactory::ok($authToken->toArray());
			return Response::json($resp);
		} else {
			$resp = RestResponseFactory::error();
			return Response::json($resp);
		}
	}
}