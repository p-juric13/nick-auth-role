# Laravel AngularJS Authentication

Includes Way Generators!

To install:

    $ mkdir path/to/src;
    $ cd path/to/src;
    $ git clone git clone git@bitbucket.org:p-juric13/nick-auth-role.git .;
    $ php artisan key:generate;
    $ composer install;
    $ bower install;
    $ npm install;
    $ grunt;
    $ sudo chmod -R 777 app/storage;
    $ composer update;

One-liner:

    mkdir laravel-angular-auth && cd laravel-angular-auth && git clone git clone git@bitbucket.org:p-juric13/nick-auth-role.git . && php artisan key:generate && composer install && bower install && npm install && grunt && composer update;


Updating composer packages without memory limit
    $ php -d memory_limit=-1 /usr/local/bin/composer update
