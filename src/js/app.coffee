"use strict"
App = angular.module("App", [
  "ngAnimate"
  "ngTouch"
  "ngSanitize"
  "ngRoute"
  "App.filters"
  "App.services"
  "App.directives"
  "App.controllers"
  "App.templates"
]).value("version", "© 2014 leduong.com").value("Domain", "http://apps.leduong.com").config([
  "$routeProvider"
  "$locationProvider"
  "$httpProvider"
  ($routeProvider, $locationProvider, $httpProvider) ->
    $routeProvider.when "/",
      templateUrl: "html/welcome.html"

    
    $routeProvider.otherwise redirectTo: "/"
    $locationProvider.html5Mode false
    $httpProvider.defaults.headers.common = "X-Requested-With": "XMLHttpRequest"
]).config([
  "$httpProvider"
  ($httpProvider) ->
    logsOutUserOn401 = [
      "$location"
      "$q"
      "Session"
      "Flash"
      ($location, $q, Session, Flash) ->
        success = (response) ->
          response

        error = (response) ->
          if response.status is 401
            Session.unset "authenticated"
            $location.path "/login.html"
            Flash.show
              type: "error"
              msg: response.data.flash

          $q.reject response

        return (promise) ->
          promise.then success, error
    ]
    $httpProvider.responseInterceptors.push logsOutUserOn401
]).config([
  "$compileProvider"
  ($compileProvider) ->
    $compileProvider.aHrefSanitizationWhitelist /^\s*(https?|mailto|tel|sms):/
]).run([
  "$rootScope"
  "$location"
  "Auth"
  "Flash"
  ($rootScope, $location, Auth, Flash) ->
    requireAuth = [
      "/account.html"
    ]
    $rootScope.isPath = ""
    $rootScope.isLoading = false
    $rootScope.$on "$routeChangeStart", (event, next, current) ->
      $rootScope.isMenu = false
      $rootScope.User = Auth.user()
      $rootScope.isLogin = Auth.isLoggedIn()
      if _(requireAuth).contains($location.path()) and not $rootScope.isLogin
        $rootScope.isPath = $location.path()
        $location.path "/login.html"
        Flash.show
          type: "info"
          msg: "Require Login."
      return
])
