angular.module("App.controllers", [])
	.controller("AppCtrl",[
		"$location"
		($location) ->

	])
	.controller("WelcomeCtrl", [
		"$rootScope"
		"$scope"
		($rootScope, $scope) ->
			$rootScope.isLoading = false
	]).controller("MessageCtrl",[
		"$scope"
		"$location"
		"$rootScope"
		"uniqueIdService"
		"$sce"
		($scope, $location, $rootScope, uniqueIdService, $sce) ->
			$scope.messages = {}
			$scope.$on "success", (event, msg) ->
				id = uniqueIdService.generate()
				$scope.messages[id] =
					class: "alert-success"
					msg: $sce.trustAsHtml(msg)

				setTimeout (->
					$scope.close id
					return
				), DELAY
				return

			$scope.$on "notify", (event, msg) ->
				id = uniqueIdService.generate()
				$scope.messages[id] =
					class: "alert-info"
					msg: $sce.trustAsHtml(msg)

				setTimeout (->
					$scope.close id
					return
				), DELAY
				return

			$scope.$on "warning", (event, msg) ->
				id = uniqueIdService.generate()
				$scope.messages[id] =
					class: "alert-warning"
					msg: $sce.trustAsHtml(msg)

				setTimeout (->
					$scope.close id
					return
				), DELAY
				return

			$scope.$on "error", (event, msg) ->
				id = uniqueIdService.generate()
				$scope.messages[id] =
					class: "alert-danger"
					msg: $sce.trustAsHtml(msg)

				setTimeout (->
					$scope.close id
					return
				), DELAY
				return

			$scope.close = (id) ->
				delete $scope.messages[id]  if $scope.messages.hasOwnProperty(id)
				return

			return
	]).controller("HeaderCtrl",[
		"securityService"
		"$scope"
		"$location"
		"$rootScope"
		(securityService, $scope, $location, $rootScope) ->
			$scope.isActive = (routePattern) ->
				return true  if (new RegExp("^" + routePattern + ".*")).test($location.path())
				false

			$scope.isAuthenticated = securityService.isAuthenticated()
			$scope.isAdmin = ->
				return true  if $scope.user.group.name is "admin"  if $scope.isAuthenticated
				false

			if $scope.isAuthenticated
				securityService.requestCurrentUser().then (user) ->
					$scope.user = user
					return

			$scope.$on "authChange", (event) ->
				$scope.isAuthenticated = securityService.isAuthenticated()
				if $scope.isAuthenticated
					securityService.requestCurrentUser().then (user) ->
						$scope.user = user
						return

				else
					$scope.user = null
				return

			return
	])