angular.module("App.services", []).factory("Flash", [
	"$rootScope"
	"$timeout"
	($rootScope, $timeout) ->
		return (
			show: (message) ->
				$rootScope.flash = message
				$timeout (->
					$rootScope.flash = ""
					return
				), 90000
				return

			clear: ->
				$rootScope.flash = ""
				return
		)
]).factory("Session", ->
	get: (key) ->
		localStorage.getItem key

	set: (key, val) ->
		localStorage.setItem key, val

	unset: (key) ->
		localStorage.removeItem key
).factory("Auth", [
	"$http"
	"$sanitize"
	"Session"
	"Flash"
	"Domain"
	($http, $sanitize, Session, Flash, Domain) ->
		sanitizeCredentials = (form) ->
			user: $sanitize(form.user)
			password: $sanitize(form.password)
			repassword: $sanitize(form.repassword)

		return (
			login: (form) ->
				$http.post(Domain + "/api/user/login", sanitizeCredentials(form)).success((data) ->
					Session.set "user", JSON.stringify(data.user)
					Flash.show
						type: "success"
						msg: data.flash

					return
				).error (data) ->
					Flash.show
						type: "danger"
						msg: data.flash

					return


			logout: ->
				$http.get(Domain + "/api/user/logout").success ->
					Session.unset "user"
					Flash.show
						type: "success"
						msg: "logout success."

					return


			update: (data) ->
				Session.set "user", JSON.stringify(data)
				return

			user: ->
				JSON.parse Session.get("user")

			isLoggedIn: ->
				Session.get "user"
		)
])
