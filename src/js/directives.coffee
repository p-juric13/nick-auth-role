
# HTML <debug val="user"></debug>

# HTML <imgmap markers="center"></imgmap>
# markers = {latitude: float, longitude: float}

#console.log('error');
angular.module("App.directives", []).directive("appVersion", [
  "version"
  (version) ->
    return (scope, elm, attrs) ->
      elm.text version
      return
]).directive("goClick", [
  "$location"
  ($location) ->
    return (scope, element, attrs) ->
      path = undefined
      attrs.$observe "goClick", (val) ->
        path = val
        return

      element.bind "click", ->
        scope.$apply ->
          $location.path path
          return

        return

      return
])